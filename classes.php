<?php
include "database.php";

class Model{
    public $table = "";
    protected $db;
    
    public function __construct(){
        $this->db = new Db();
    }
    
    public function all(){
        return $this->db->query("SELECT * FROM {$this->table}");
    }
    
    public function find($id){
        $rs = $this->db->query("SELECT * FROM {$this->table} WHERE id = '{$id}'");
        return $rs->fetch();
    }
}

class Student extends Model{
    public $table = "students";
}

$s = new Student();
//$rs = $s->all();
//while($r = $rs->fetch()){
//    echo $r['name']."<br>";
//}

$student = $s->find(1);
echo $student['name'];