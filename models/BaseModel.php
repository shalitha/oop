<?php
require_once "libs/database.php";

class BaseModel{
    public static $table = "";
    protected static $db;

    public function __construct(){
        Static::$db = new Db();
    }
    
    
    public static function all(){
        Static::$db = new Db();
        return Static::$db->query("SELECT * FROM 
            ".Static::$table);
    }
    
    public static function find($id){
        Static::$db = new Db();
        $rs = Static::$db->query("SELECT * 
            FROM ".Static::$table." WHERE id = '{$id}'");
        return $rs->fetch();
    }

    public static function delete($id){
        Static::$db = new Db();
        return Static::$db->query("DELETE 
            FROM ".Static::$table." WHERE id = '{$id}'");
    }

    public static function where($where_cl){
        Static::$db = new Db();
        return Static::$db->query("SELECT * FROM 
            ".Static::$table." WHERE ".$where_cl);
    }

    public function insert(){
    }
}