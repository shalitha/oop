<!DOCTYPE html>
<html>
<head>
	<title>Opp example</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<table class="table">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Country</th>
			</tr>
		<?php
		require "models/StudentModel.php";
		$students = StudentModel::all();
		while($row = $students->fetch()){ ?>
			<tr>
				<td><?= $row['id'] ?></td>
				<td><?= $row['name'] ?></td>
				<td><?= $row['country'] ?></td>
			</tr>
			<?php } ?>
		</table>	
	</div>
	

</body>
</html>